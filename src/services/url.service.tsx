import axios, { AxiosInstance} from 'axios';

export const BASE_URL = 'https://reqres.in/api';

// Create HTTP instance
export const https: AxiosInstance = axios.create({
  baseURL: BASE_URL,
 
});

