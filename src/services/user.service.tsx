import { AxiosResponse } from 'axios';
import { https } from './url.service';

interface User {
  email: string;
  password: string;
}

export const Usersev = {
  postLogin: (email: string, password: string): Promise<AxiosResponse> => {
    return https.post('/login', { email, password });
  },
  getUser: (): Promise<AxiosResponse<User[]>> => {
    return https.get('/users?page=2');
  },
};

export default Usersev;