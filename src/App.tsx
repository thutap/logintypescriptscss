import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { routes } from './router/routes';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {routes.map(({ path, component }, index) => (
            <Route key={index} path={path} element={component} />
          ))}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;