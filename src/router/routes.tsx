import React from 'react';
import LoginPage from '../Page/Login';
import UserPage from '../Page/UserPage';

interface Route {
  path: string;
  component: React.ReactElement;
}

export const routes: Route[] = [
  {
    path: '/',
    component: <LoginPage />,
  },
  {
    path: '/home',
    component: <UserPage />,
  },
];