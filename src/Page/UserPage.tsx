import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import UserService from '../services/user.service';
import { useNavigate } from 'react-router-dom';
import './User.css';
interface User {
  avatar: string;
  email: string;
  first_name: string;
  last_name: string;
}
interface ResponseData {
  data: any; // Thay 'any' bằng kiểu dữ liệu chính xác của `data` nếu bạn biết nó.
  // Các thuộc tính khác trong phản hồi API cũng có thể được định nghĩa ở đây.
}
const UserPage: React.FC = () => {
  const [users, setUsers] = useState<User[]>([]);
  const navigate = useNavigate();


  useEffect(() => {
    const fetchData = async () => {
      try {
        const res: ResponseData = await UserService.getUser(); // Xác định kiểu cho biến res
        if (typeof res.data === 'object' && res.data.hasOwnProperty('data')) {
          const responseData = res.data.data; // Access the nested data property
          if (Array.isArray(responseData)) {
            const userList: User[] = responseData.map((item: any) => ({
              avatar: item.avatar,
              email: item.email,
              first_name: item.first_name,
              last_name: item.last_name
            }));
            setUsers(userList);
          } else {
            console.error('Response data is not an array:', responseData);
            // setError("Error fetching user data"); // Uncomment this line if you plan to use 'error'
          }
        } else {
          console.error('Property data does not exist on response:', res.data);
          // setError("Error fetching user data"); // Uncomment this line if you plan to use 'error'
        }
      } catch (error) {
        console.error('Error fetching data:', error);
        // setError("Error fetching user data"); // Uncomment this line if you plan to use 'error'
      } finally {
        // setIsLoading(false); // Uncomment this line if you plan to use 'isLoading'
      }
    };
  
    const isLoggedIn = localStorage.getItem('isLoggedIn');
    if (!isLoggedIn) {
      navigate('/');
    } else {
      fetchData();
    }
  }, [navigate]);
  
  const handleLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('isLoggedIn');
    navigate('/');
  };

  const renderUser = () => {
    return users.map((user, index) => (
      <div key={index}>
        <div className='form-content'>
          <div className='form-img'>
            <img src={user.avatar} alt='' />
          </div>
          <div className='form-text'>
            <p>{user.email}</p>
            <p>{user.first_name}</p>
            <p>{user.last_name}</p>
          </div>
        </div>
      </div>
    ));
  };

  return (
    <div>
      <h1>User</h1>
      <Button type='primary' onClick={handleLogout}>
        Logout
      </Button>
      <div className='form'>{renderUser()}</div>
    </div>
  );
};

export default UserPage;